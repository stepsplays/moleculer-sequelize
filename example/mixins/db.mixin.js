"use strict";


const MoleculerSequelize = require("../../index");
const Sequelize = require("sequelize");
const sqlite3 = require("sqlite3");

const DatabaseMixin = {
	mixins: [MoleculerSequelize],
	settings: {
		sequelize: new Sequelize("database_development", "root", null, {dialect: "sqlite", storage: "database.sqlite"}),

	},
};

module.exports = DatabaseMixin;
