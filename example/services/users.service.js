"use strict";

const Sequelize = require("sequelize");
const SequelizeMixin = require("../mixins/db.mixin");

module.exports = {
	name: "users",
	mixins: [SequelizeMixin],
	model: {
		name: "users",
		define: {
			id: {
				primaryKey: true,
				autoIncrement: true,
				type: Sequelize.INTEGER
			},
			name: Sequelize.STRING,
			email: Sequelize.STRING,
			password: Sequelize.STRING,
		},
		association: [{
			model: "items",
			relationship: "hasMany",
			foreignKey: 'userTable',
		},],
		include: ["items"],
		options: {
			paranoid: true,
		}
	},
	afterConnected() {
		console.log("user model connected");
	},
	actions: {
		list: {
			include: ["items"]
		},
		get: {
			include: ["items"]
		},
	},
};
