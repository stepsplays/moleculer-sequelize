# Moleculer Sequelize
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
# Sequelize plugin for Moleculer.js Framework
If you like to use Sequelize and use models in your backend services, but you like to have relationships between models, this is a plugin for you.

### Configuration
Create Simple Mixin to use with every Service-Models.
```javascript
const MoleculerSequelize = require("moleculer-sequelize");
const SequelizeMixin = {
    mixins: [MoleculerSequelize],
    settings: {
        sequelize: new Sequelize( "database_development", "root", "123456",{
            host: "127.0.0.1",
            dialect: "mysql",
        }),
        
    },
};
module.exports = SequelizeMixin;
```
### Service-Model definition
```javascript
const Sequelize = require("sequelize");
const SequelizeMixin = require("./sequelize.mixin");

module.exports = { 
    name: "users",
    mixins: [SequelizeMixin],
    model: {
        name: "users",
        define: {
            id: {
                primaryKey: true,
                autoIncrement: true,
                type: Sequelize.INTEGER
            },
            name: Sequelize.STRING,
            email: Sequelize.STRING,
            password: Sequelize.STRING,
        },
    }
}
```
### CRUD operations
Create record;
```javascript
broker.call("users.create", {name:"foo", email:" foo@bar.com", password: "123456"});
//or use API Gateway integration 'POST /api/users' and pass form in body

```
To find by id;
```javascript
broker.call("users.get", {id:1});
//or use API Gateway integration 'GET /api/users/1'

```
To find all;
```javascript
broker.call("users.list", {query: {name: "foo"}});
//or use API Gateway integration 'GET /api/users'
```

### Relationships
Its possible to create Relitionships between tables, just adding association key with name of other model and relationType(hasOne, hasMany, belongsTo, belongsToMany)
```javascript
const Sequelize = require("sequelize");
const SequelizeMixin = require("./sequelize.mixin");

module.exports = { 
    name: "users",
    mixins: [SequelizeMixin],
    model: {
        name: "users",
        define: {
            id: {
                primaryKey: true,
                autoIncrement: true,
                type: Sequelize.INTEGER
            },
            name: Sequelize.STRING,
            email: Sequelize.STRING,
            password: Sequelize.STRING,
        },
        associations: [{
            model: "items",
            relationship: "hasMany",
        },],
    }
}
```
And to integrate this to CRUD actions just add 'includes' in service-model action
```javascript
const Sequelize = require("sequelize");
const SequelizeMixin = require("./sequelize.mixin");

module.exports = { 
    name: "users",
    mixins: [SequelizeMixin],
    actions:{
        findOne:{
            includes: ['items']
        }
    }
}
```
and Will return
```json
{
  "name": "foo",
  "email": "foo@bar.com",
  "password": "123456",
  "items": [{
    "name": "bar"  
  }]
}
```
 


