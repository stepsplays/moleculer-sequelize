"use strict";
const path = require("path");
const _ = require("lodash");
const Sequelize = require("sequelize");
const Op = require("sequelize").Op;
const {MoleculerClientError, EntityNotFoundError} = require("moleculer").Errors;

let sequelize;

const MoleculerSequelize = {
    model: {},
    sequelize: {},
    settings: {
        settings: {
            pageSize: 10,
            maxPageSize: 100,
            maxLimit: -1,
        },
    },
    actions: {
        get: {
            cache: {
                keys: ["id", "populate", "fields", "mapping"]
            },
            rest: "GET /:id",
            params: {
                id: [
                    {type: "string",  optional: true},
                    {type: "number",  optional: true},
                    {type: "array",  optional: true}
                ],
                populate: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"},
                ],
                fields: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"},
                ],
                exclude: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"},
                ],
                mapping: {type: "boolean", optional: true},
                include: [
                    {type: "object", optional: true},
                    {type: "string", optional: true},
                    {type: "array", optional: true},
                ],
            },
            async handler(ctx) {
                let params = this.sanitizeParams(ctx, ctx.params);
                params = await this.queryBuilder(ctx, "get", params);
                let id = params.id;
                params.where = params.query;
                delete params.exclude;
                delete params.fields;
                delete params.query;
                delete params.id;
                if (Array.isArray(id)) {
                    return this.model.findAll(params);
                } else {
                    params.limit = 1;
                    return this.model.findAll(params).then(doc => {
                        if (!doc.length) return Promise.reject(new MoleculerClientError(this.notFoundError(), 404));
                        return doc[0];
                    });
                }
            }
        },
        list: {
            rest: "GET /",
            params: {
                populate: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"},
                ],
                fields: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"},
                ],
                exclude: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"},
                ],
                page: {type: "number", integer: true, min: 1, optional: true, convert: true},
                pageSize: {type: "number", integer: true, min: 0, optional: true, convert: true},
                sort: {type: "string", optional: true},
                search: [
                    {type: "object", optional: true},
                    {type: "string", optional: true},
                ],
                searchFields: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"},
                ],
                query: [
                    {type: "object", optional: true},
                    {type: "string", optional: true},
                ],
                include: [
                    {type: "string", optional: true},
                    {type: "object", optional: true},
                    {type: "array", optional: true},
                ],
            },
            async handler(ctx) {
                let params = this.sanitizeParams(ctx, ctx.params);
                let countParams = Object.assign({}, params);
                // Remove pagination params
                if (countParams && countParams.limit)
                    countParams.limit = null;
                if (countParams && countParams.offset)
                    countParams.offset = null;
                if (params.limit == null) {
                    if (this.settings.limit > 0 && params.pageSize > this.settings.limit)
                        params.limit = this.settings.limit;
                    else
                        params.limit = params.pageSize;
                }
                let query = await this.queryBuilder(ctx, "list", params);
                return Promise.all([
                    // Get rows
                    this.createCursor(query),
                    // Get count of all rows
                    this.createCursor(Object.assign(countParams, query), true)
                ]).then(res => {
                    return this.transformDocuments(ctx, query, res[0])
                        .then(docs => {
                            return {
                                rows: docs,
                                total: res[1],
                                page: query.page,
                                pageSize: query.pageSize,
                                totalPages: Math.floor((res[1] + query.pageSize - 1) / query.pageSize) || 1
                            };
                        });
                });
            }
        },
        find: {
            cache: {
                keys: ["populate", "fields", "limit", "offset", "sort", "search", "searchFields", "query"]
            },
            params: {
                populate: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"},
                ],
                fields: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"},
                ],
                exclude: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"},
                ],
                limit: {type: "number", integer: true, min: 0, optional: true, convert: true},
                offset: {type: "number", integer: true, min: 0, optional: true, convert: true},
                sort: {type: "string", optional: true},
                search: [
                    {type: "object", optional: true},
                    {type: "string", optional: true},
                ],
                searchFields: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"},
                ],
                query: [
                    {type: "object", optional: true},
                    {type: "string", optional: true},
                ],
                include: [
                    {type: "string", optional: true},
                    {type: "object", optional: true},
                    {type: "array", optional: true},
                ],
            },
            async handler(ctx) {
                let params = this.sanitizeParams(ctx, ctx.params);
                let query = await this.queryBuilder(ctx, "find", params);
                return this.createCursor(query);
            }
        },
        create: {
            rest: "POST /",
            async handler(ctx) {
                let {beforeCreate, afterCreate, include, entity,} = this.schema.actions.create;
                if (entity) {
                    for (let key in entity) {
                        if (key.startsWith("$")) {
                            entity[key.substring(1)] = eval(entity[key]);
                            delete entity[key];
                        }
                    }
                    Object.assign(ctx.params, entity);
                }
                if (beforeCreate) {
                    await beforeCreate.call(this, ctx);
                }
                let model = await this.model.create(ctx.params)
                    .then(json => this.entityChanged("created", json, ctx).then(() => json));
                await this.associationCreate(model, ctx.params, include);
                if (afterCreate) {
                    await afterCreate.call(this, ctx, model);
                }
                let fields = this.schema.actions.create.fields || this.settings.fields;
                let exclude = this.schema.actions.create.exclude || this.settings.exclude || [];
                if (fields) {
                    for (let key in model.dataValues) {
                        if (!fields.includes(key)) {
                            delete model.dataValues[key];
                        }
                    }
                } else if (exclude) {
                    for (let key in model.dataValues) {
                        if (exclude.includes(key)) {
                            delete model.dataValues[key];
                        }
                    }
                }
                return model;
            }
        },
        insert: {
            params: {
                entity: {type: "object", optional: true},
                entities: {type: "array", optional: true}
            },
            async handler(ctx) {
                let params = this.sanitizeParams(ctx, ctx.params);
                let {beforeCreate, afterCreate} = this.schema.actions.insert;
                if (beforeCreate) {
                    await beforeCreate.call(this, ctx);
                }
                let entity;
                if (Array.isArray(params.entities)) {
                    entity = await this.validateEntity(params.entities);
                    await this.model.bulkCreate(entity);

                } else if (params.entity) {
                    entity = await this.validateEntity(params.entity);
                    await this.model.create(entity);
                }
                if (!entity) return Promise.reject(new MoleculerClientError("Invalid request! The 'params' must contain 'entity' or 'entities'!", 400));
                if (afterCreate) {
                    await afterCreate.call(this, ctx, entity);
                }
                this.clearCache();
                return entity;
            }
        },
        update: {
            rest: "PUT /:id",
            async handler(ctx) {
                let params = ctx.params;
                let {beforeUpdate, afterUpdate, include, query, entity} = this.schema.actions.update;
                let queryParams = await this.queryBuilder(ctx, "update", {query: {id: params.id}});

                let model = await this.model.findOne({where: queryParams.query, include: include || []});
                if (!model) throw  new MoleculerClientError(this.notFoundError(), 404);
                if (beforeUpdate) {
                    await beforeUpdate.call(this, ctx, model);
                }
                await model.update(params);

                await this.associationCreate(model, ctx.params, include || [], true);
                let fields = this.schema.actions.update.fields || this.settings.fields;
                let exclude = this.schema.actions.update.exclude || this.settings.exclude || [];
                if (afterUpdate) {
                    await afterUpdate.call(this, ctx, model);
                }
                if (fields) {
                    for (let key in model.dataValues) {
                        if (!fields.includes(key)) {
                            delete model.dataValues[key];
                        }
                    }
                } else if (exclude) {
                    for (let key in model.dataValues) {
                        if (exclude.includes(key)) {
                            delete model.dataValues[key];
                        }
                    }
                }
                return model;
            }
        },
        remove: {
            rest: "DELETE /:id",
            params: {
                id: {type: "any"}
            },
            async handler(ctx) {
                let params = this.sanitizeParams(ctx, ctx.params);
                let {beforeRemove, afterRemove, query} = this.schema.actions.remove;
                let queryParams = await this.queryBuilder(ctx, "remove", {query: {id: params.id}});

                let model = await this.model.findOne({where: queryParams.query});
                if (!model) throw new MoleculerClientError(this.notFoundError(), 404);

                if (beforeRemove) {
                    await beforeRemove.call(this, ctx, model);
                }
                await model.destroy();
                this.clearCache();

                if (afterRemove) {
                    await afterRemove.call(this, ctx, model);
                }
                let fields = this.schema.actions.remove.fields || this.settings.fields;
                let exclude = this.schema.actions.remove.exclude || this.settings.exclude || [];
                if (fields) {
                    for (let key in model.dataValues) {
                        if (!fields.includes(key)) {
                            delete model.dataValues[key];
                        }
                    }
                } else if (exclude) {
                    for (let key in model.dataValues) {
                        if (exclude.includes(key)) {
                            delete model.dataValues[key];
                        }
                    }
                }
                return model;
            }
        },
        removeWhere: {
            params: {
                query: [{type: "string", optional: true}, {type: "object", optional: true},],
                where: [{type: "string", optional: true}, {type: "object", optional: true},],
            },
            async handler(ctx) {
                let params = this.sanitizeParams(ctx, ctx.params);
                if (!params.where) params.where = params.query;
                delete params.query;
                let result = await this.model.destroy(params);
                this.clearCache();
                return result;
            }
        },
        count: {
            cache: {
                keys: ["search", "searchFields", "query"]
            },
            params: {
                search: {type: "string", optional: true},
                searchFields: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"},
                ],
                query: [
                    {type: "object", optional: true},
                    {type: "string", optional: true},
                ],
            },
            handler(ctx) {
                let params = this.sanitizeParams(ctx, ctx.params);
                if (params && params.limit)
                    params.limit = null;
                if (params && params.offset)
                    params.offset = null;
                return this.model.count(params);
            }
        },

        tableInfo: {
            handler() {
                let {options, associations, tableName} = this.model;
                options = {...options};
                associations = {...associations};
                for (let item in associations) {
                    associations[item] = {...associations[item]};
                    delete associations[item].sequelize;
                    delete associations[item].source;
                    delete associations[item].target;
                    associations[item].options = {...associations[item].options};
                    delete associations[item].options.sequelize;
                }
                delete options.sequelize;
                return {options, associations, tableName};
            }
        }
    },


    async started() {
        if (!sequelize) {
            sequelize = this.settings.sequelize;
        }

        let model = this.schema.model || {};
        const Model = sequelize.define(model.name, model.define, model.options);
        model.association = model.association || [];
        Model.associate = function (models) {
            for (let associate of model.association) {
                let opts = {...associate};
                delete opts.model;
                delete opts.relationship;

                Model[associate.relationship](models[associate.model], opts);
            }
        };
        if (!sequelize.models) {
            sequelize.models = {};
        }
        sequelize.models[model.name] = Model;
        this.model = Model;
        this.sequelize = sequelize;
        this.connect();
    },
    merged(schema) {
        let cleanCache = (ctx) => {
            this.clearCache(false);
        };
        let createCacheEvents = (association, events) => {
            if (Array.isArray(association)) {
                for (let item of association) {
                    createCacheEvents(item, events);
                }
            } else if (typeof association === "object") {
                events["cache.clean." + association.model] = cleanCache;
                if (association.association) createCacheEvents(association.association, events);
            } else if (typeof association === "string") {
                events["cache.clean." + association] = cleanCache;
            }
        };
        createCacheEvents(schema.model.association, schema.events);
        if (schema.settings.params) {
            let {$$strict, ...params} = schema.settings.params;
            params = {...params};
            let directives = {};
            for (let item in params) {
                if (item.startsWith("$")) {
                    directives[item.substring(1)] = params[item];
                    delete params[item];
                }
            }
            if (!directives.create) directives.create = {};
            if (!directives.update) directives.update = {id: {type: "number", convert: true, integer: true}};

            if (!directives.update.id) directives.update.id = {type: "number", convert: true, integer: true};

            for (let directive in directives) {
                if (schema.actions[directive]) {
                    schema.actions[directive].params = Object.assign({}, params, directives[directive]);
                    schema.actions[directive].params.$$strict = $$strict;
                }
            }

            if (schema.actions.update.params.$optional) {
                delete schema.actions.update.params.$optional;
                let updateParams = schema.actions.update.params;
                for (let item in updateParams) {
                    if (item !== "id" && !item.startsWith("$$")) {
                        if (typeof updateParams[item] == "object") {
                            updateParams[item].optional = true;
                        } else {
                            updateParams[item] = {type: updateParams[item], optional: true};
                        }
                    }
                }
            }
        }
    },
    methods: {
        /**
         * Connect to database.
         */
        connect(retry = 0) {
            return this.sequelize.authenticate().then(async () => {
                if (!this.settings.noSync) {
                    try {
                        await this.model.sync();
                    } catch (e) {
                        console.log(e);
                        if (retry === 0) return this.connect(1);
                    }
                }
                if (typeof (this.schema.afterConnected) === "function") {
                    try {
                        await this.schema.afterConnected.call(this);
                    } catch (err) {
                        this.logger.error("afterConnected error!", err);
                    }
                }
            }).catch(console.log);
        },

        /**
         * Disconnect from database.
         */
        disconnect() {
            if (_.isFunction(this.adapter.disconnect))
                return this.adapter.disconnect();
        },

        async associationCreate(model, form, associations, edit = false) {
            if (!form || !associations) {
                return;
            }
            if (Array.isArray(associations)) {
                for (let item of associations) {
                    if (typeof item === "string") {
                        await this.associationCreate(model, form, item, edit);
                    } else {
                        await this.associationCreate(model, form, item, edit);
                    }
                }
            } else if (typeof associations === "object") {
                let associationName = associations.association || associations.model;
                let as = associations.as || associationName;
                let service = associations.service || associationName || associations.as;
                let newModel;
                let modelSchema = this.schema.model || {};
                let modelAssociations = modelSchema.associations || [];
                let modelAssosiation = modelAssociations.find(value => value.model === association) || {};
                let tableSchema = await this.broker.call(service + ".tableInfo");
                let parentAssociation = tableSchema.associations[model.$parentAssociation] || {};
                let foreignKey = modelAssosiation.foreignKey
                    || parentAssociation.foreignKey
                    || (model.constructor.options.name.singular + "Id");
                if (edit && !associations.unique) {
                    await this.broker.call(service + ".removeWhere", {query: {[foreignKey]: model.id}});
                }
                if (Array.isArray(form[as])) {
                    for (let item of form[as]) {
                        item[foreignKey] = model.id;
                        delete item.id;
                    }
                    if (associations.unique && edit) {
                        let tempForm = [...form[as]];
                        newModel = [];
                        for (let item of model[as]) {
                            let itemFind = tempForm.find(value => value.id === item.id);
                            if (itemFind) {
                                tempForm.slice(tempForm.indexOf(itemFind), 1);
                                newModel.push([...(await this.broker.call(service + ".update", itemFind))]);
                            } else {
                                await this.broker.call(service + ".remove", {id: item.id});
                            }
                        }
                        for (let item of tempForm) {
                            newModel.push([...(await this.broker.call(service + ".insert", {entities: tempForm}))]);
                        }
                        for (let item of newModel) {
                        }
                    } else {
                        newModel = await this.broker.call(service + ".insert", {entities: form[as]});
                    }
                } else {
                    if (associations.unique && edit) {
                        newModel = await this.broker.call(service + ".update", form[as]);
                    } else {
                        form[as][foreignKey] = model.id;
                        delete form[as].id;
                        newModel = await this.broker.call(service + ".create", form[as]);
                    }
                }
                if (associations.include) {
                    if (Array.isArray(newModel)) {
                        for (let model of newModel) {
                            model.$parentAssociation = tableInfo.options.name.singular;
                        }
                        for (let i = 0; i < newModel.length; i++) {
                            await this.associationCreate(newModel[i], form[as][i], associations.include, edit);
                        }
                    } else {
                        newModel.$parentAssociation = tableSchema.options.name.singular;
                        await this.associationCreate(newModel, form[as], associations.include, edit);
                    }
                }
            } else {
                let association = associations;
                let modelSchema = this.schema.model || {};
                let modelAssociations = modelSchema.associations || [];
                let modelAssosiation = modelAssociations.find(value => value.model === association) || {};
                let foreignKey = modelAssosiation.foreignKey || (model.constructor.options.name.singular + "Id");
                try {
                    if (edit) {
                        await this.broker.call(association + ".removeWhere", {query: {[foreignKey]: model.id}});
                    }
                    if (Array.isArray(form[association])) {
                        for (let item of form[association]) {
                            item[foreignKey] = model.id;
                        }
                        await this.broker.call(association + ".insert", {entities: form[association]});
                    } else {
                        form[foreignKey] = model.id;
                        await this.broker.call(association + ".create", form[association]);
                    }
                } catch (e) {
                    console.log(e);
                }
            }
        },

        async queryBuilder(ctx, actionName, params) {
            let query = {...params};
            let action = this.schema.actions[actionName];
            //Sanitarize dinamic query
            params.query = params.query || {};
            if (params.id) {
                params.query.id = params.id;
            }
            query.fields = action.fields || this.settings.fields || params.fields;
            query.exclude = action.exclude || this.settings.exclude || params.exclude;
            query.include = action.include || this.settings.include || params.include;
            query.query = Object.assign(params.query || {}, action.query || this.settings.query || {});
            for (let key in query.query) {
                if (key.startsWith("$")) {
                    query.query[key.substring(1)] = eval(query.query[key]);
                    delete query.query[key];
                } else if (typeof query.query[key] === "object" && !Array.isArray(query.query[key])) {
                    for (let innerKey in query.query[key]) {
                        let field = this.model.associations[key].target.rawAttributes[innerKey].field;
                        query.query[`$${key}.${field}$`] = query.query[key][innerKey];
                    }
                    delete query.query[key];
                }
            }

            if (action.queryHandler) {
                let funcResult = await (action.queryHandler.call(this, ctx, query))
                query = funcResult || query;
            }
            if (query.fields) {
                query.attributes = query.fields;
            } else if (query.exclude) {
                query.attributes = {exclude: query.exclude};
            }
            delete query.exclude;
            delete query.fields;
            return query;
        },

        transformDocuments(ctx, params, docs) {
            let isDoc = false;
            if (!Array.isArray(docs)) {
                if (_.isObject(docs)) {
                    isDoc = true;
                    docs = [docs];
                } else
                    return Promise.resolve(docs);
            }

            return Promise.resolve(docs)

                // Convert entity to JS object
                .then(docs => docs.map(doc => this.entityToObject(doc)))
                .then(json => {
                    let fields = ctx && params.fields ? params.fields : this.settings.fields;
                    // Compatibility with < 0.4
                    /* istanbul ignore next */
                    if (_.isString(fields))
                        fields = fields.split(" ");

                    // Authorize the requested fields
                    const authFields = this.authorizeFields(fields);

                    return json.map(item => this.filterFields(item, authFields));
                })

                // Return
                .then(json => isDoc ? json[0] : json);
        },
        authorizeFields(fields) {
            if (this.settings.fields && this.settings.fields.length > 0) {
                let res = [];
                if (Array.isArray(fields) && fields.length > 0) {
                    fields.forEach(f => {
                        if (this.settings.fields.indexOf(f) !== -1) {
                            res.push(f);
                            return;
                        }
                        if (f.indexOf(".") !== -1) {
                            let parts = f.split(".");
                            while (parts.length > 1) {
                                parts.pop();
                                if (this.settings.fields.indexOf(parts.join(".")) !== -1) {
                                    res.push(f);
                                    break;
                                }
                            }
                        }
                        let nestedFields = this.settings.fields.filter(prop => prop.indexOf(f + ".") !== -1);
                        if (nestedFields.length > 0) {
                            res = res.concat(nestedFields);
                        }
                    });
                    //return _.intersection(f, this.settings.fields);
                }
                return res;
            }

            return fields;
        },
        createCursor(params, isCounting) {
            if (!params) {
                if (isCounting)
                    return this.model.count();

                return this.model.findAll();
            }
            const q = {
                where: {}
            };
            // Text search
            if (typeof params.search === "string" && params.search !== "") {
                let fields = Object.keys(this.model.rawAttributes);
                if (params.searchFields) {
                    fields = _.isString(params.searchFields) ? params.searchFields.split(" ") : params.searchFields;
                }

                const searchConditions = fields.map(f => {
                    return {
                        [f]: {
                            [Op.like]: "%" + params.search + "%"
                        }
                    };
                });
                if (params.query) {
                    q.where[Op.and] = [
                        params.query,
                        {[Op.or]: searchConditions}
                    ];
                } else {
                    q.where[Op.or] = searchConditions;
                }
            } else if (typeof params.search === "object" && Object.keys(params.search).length) {
                let searchConditions = [];
                for (let item in params.search) {
                    let fieldName = item.includes(".") ? ("$" + item + "$") : item;
                    searchConditions.push({[fieldName]: {[Op.like]: "%" + params.search[item] + "%"}});
                }
                if (params.query) {
                    q.where[Op.and] = [
                        params.query,
                        {[Op.or]: searchConditions}
                    ];
                } else {
                    q.where[Op.or] = searchConditions;
                }
            } else if (params.query) {
                Object.assign(q.where, params.query);
            }

            // Sort
            if (params.sort) {
                let sort = this.transformSort(params.sort);
                if (sort)
                    q.order = sort;
            }


            // Pananoid
            if (params.paranoid !== undefined) q.paranoid = !!params.paranoid;

            // Offset
            if (_.isNumber(params.offset) && params.offset > 0)
                q.offset = params.offset;

            // Limit
            if (_.isNumber(params.limit) && params.limit > 0)
                q.limit = params.limit;

            if (params.include)
                q.include = params.include;
            if (params.attributes)
                q.attributes = params.attributes;

            if (isCounting)
                return this.model.count(q);
            return this.model.findAll(q);
        },
        sanitizeParams(ctx, params) {
            let p = Object.assign({}, params);

            // Convert from string to number
            if (typeof (p.limit) === "string")
                p.limit = Number(p.limit);
            if (typeof (p.offset) === "string")
                p.offset = Number(p.offset);
            if (typeof (p.page) === "string")
                p.page = Number(p.page);
            if (typeof (p.pageSize) === "string")
                p.pageSize = Number(p.pageSize);
            // Convert from string to POJO
            if (typeof (p.query) === "string")
                p.query = JSON.parse(p.query);
            if (typeof (p.search) === "string") {
                try {
                    p.search = JSON.parse(p.search);
                } catch (e) {
                }
            }

            if (typeof (p.sort) === "string")
                p.sort = p.sort.replace(/,/g, " ").split(" ");

            if (typeof (p.fields) === "string")
                p.fields = p.fields.replace(/,/g, " ").split(" ");

            if (typeof (p.populate) === "string")
                p.populate = p.populate.replace(/,/g, " ").split(" ");

            if (typeof (p.searchFields) === "string")
                p.searchFields = p.searchFields.replace(/,/g, " ").split(" ");

            if (ctx.action.name.endsWith(".list")) {
                // Default `pageSize`
                if (!p.pageSize)
                    p.pageSize = this.settings.pageSize;

                // Default `page`
                if (!p.page)
                    p.page = 1;

                // Limit the `pageSize`
                if (this.settings.maxPageSize > 0 && p.pageSize > this.settings.maxPageSize)
                    p.pageSize = this.settings.maxPageSize;

                // Calculate the limit & offset from page & pageSize
                p.limit = p.pageSize;
                p.offset = (p.page - 1) * p.pageSize;
            }
            // Limit the `limit`
            if (this.settings.maxLimit > 0 && p.limit > this.settings.maxLimit)
                p.limit = this.settings.maxLimit;

            return p;
        },
        transformSort(paramSort) {
            let sort = paramSort;
            if (_.isString(sort))
                sort = sort.replace(/,/, " ").split(" ");

            if (Array.isArray(sort)) {
                let sortObj = [];
                sort.forEach(s => {
                    let splitedSort = s.split(".");
                    if (splitedSort[0].startsWith("-")) {
                        splitedSort[0] = splitedSort[0].slice(1);
                        sortObj.push([...splitedSort, "DESC"]);
                    } else {
                        sortObj.push([...splitedSort, "ASC"]);
                    }
                });
                return sortObj;
            }

            if (_.isObject(sort)) {
                return Object.keys(sort).map(name => [name, sort[name] > 0 ? "ASC" : "DESC"]);
            }

            /* istanbul ignore next*/
            return [];
        },
        notFoundError() {
            let name = this.model.name;
            if (name.charAt(name.length - 1) === "s") {
                name = name.substring(0, name.length - 1);
            }
            return name.toUpperCase() + "_NOT_FOUND";
        },
        entityToObject(entity) {
            return entity.get({plain: true});
        },
        validateEntity(entity) {
            if (!_.isFunction(this.settings.entityValidator))
                return Promise.resolve(entity);

            let entities = Array.isArray(entity) ? entity : [entity];
            return Promise.all(entities.map(entity => this.settings.entityValidator.call(this, entity))).then(() => entity);
        },
        filterFields(doc, fields) {
            // Apply field filter (support nested paths)
            if (Array.isArray(fields)) {
                let res = {};
                fields.forEach(n => {
                    const v = _.get(doc, n);
                    if (v !== undefined)
                        _.set(res, n, v);
                });
                return res;
            }

            return doc;
        },

        entityChanged(type, json, ctx) {
            return this.clearCache().then(() => {
                const eventName = `entity${_.capitalize(type)}`;
                if (this.schema[eventName] != null) {
                    return this.schema[eventName].call(this, json, ctx);
                }
            });
        },
        /**
         * Clear cached entities
         *
         * @methods
         * @returns {Promise}
         */
        clearCache(broadcast = true) {
            if (broadcast) this.broker.broadcast(`cache.clean.${this.fullName}`);
            if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
            return Promise.resolve();
        },
        async seedDatabase(arr) {
            await this.model.bulkCreate(arr, {ignoreDuplicates: true});
        }
    },
    events: {
        async "$services.changed"() {
            await this.model.associate(sequelize.models);
        },
        "seedDb": {
            params: {
                all: {type: "boolean", default: false},
                db: {type: "string", optional: true}
            },
            async handler(ctx) {
                let dir = path.resolve(`seeds/${this.name}.js`);
                if (ctx.params.all) {
                    try {
                        const dbData = require(dir);
                        await this.seedDatabase(dbData);
                    } catch (e) {
                        //TODO: Do a better error feedback.
                        console.log(404, "FILE NOT FOUND: ", dir);
                    }
                } else if (ctx.params.db === this.name) {
                    const dbData = require(dir);
                    await this.seedDatabase(dbData);
                }
            }
        }
    }
};


module.exports = MoleculerSequelize;
